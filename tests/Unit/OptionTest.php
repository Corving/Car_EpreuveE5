<?php


use App\Models\Option;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class OptionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function testCreateOption()
    {
        $optionCount = (DB::table('options')->count()) + 1;
        $option = Option::create(["libelle"=>'Feux halogène']);
        $this->assertEquals($optionCount, Option::count());
        return $option->id;
    }


    /**
     * @depends testCreateOption
     */

    public function testUpdateOption($id)
    {
        $optionCount = (DB::table('options')->count());
        $option = Option::findOrFail($id);
        $option->update(["libelle"=>'Feux LED']);
        $this->assertEquals($optionCount, Option::count());
        return $option->id;

    }

    /**
     * @depends testUpdateOption
     */


    public function testDeleteOption($id)
    {
        Option::findOrFail($id)->delete();
    }

}
