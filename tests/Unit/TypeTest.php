<?php


use App\Models\Type;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class TypeTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function testCreateType()
    {
        $typeCount = (DB::table('types')->count()) + 1;
        $type = Type::create(["libelle"=>'coucou']);
        $this->assertEquals($typeCount, Type::count());
        return $type->id;

    }



    /**
     * @depends testCreateType
     */

    public function testUpdateType($id)
    {
        $typeCount = (DB::table('types')->count());
        $type = Type::findOrFail($id);
        $type->update(["libelle"=>'4x4']);
        $this->assertEquals($typeCount, Type::count());
        return $type->id;

    }

    /**
     * @depends testUpdateType
     */


    public function testDeleteType($id)
    {
        Type::findOrFail($id)->delete();
    }


}
