<?php


use App\Models\Car;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class CarTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function testCreateCar()
    {
        $carCount = (DB::table('cars')->count()) + 1;
        $car = Car::create(['immatriculation' => 'df992az', 'marque' => 'FORD', 'modele' => 'FIESTA', 'couleur' => 'Vert', 'type_id' => 5]);
        $car->options()->sync([1]);
        $this->assertEquals($carCount, Car::count());
        return $car->id;
    }


    /**
     * @depends testCreateCar
     */

    public function testUpdateCar($id)
    {
        $carCount = (DB::table('cars')->count());
        $car = Car::findOrFail($id);
        $car->update(['immatriculation' => 'df100az', 'marque' => 'FORD', 'modele' => 'FIESTA', 'couleur' => 'Rouge', 'type_id' => 4]);
        $car->options()->sync([2]);
        $this->assertEquals($carCount, Car::count());
        return $car->id;

    }

}
