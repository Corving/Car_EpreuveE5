<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\CarController;
use \App\Http\Controllers\TypeController;
use \App\Http\Controllers\OptionController;
use \App\Http\Controllers\PiloteController;

Route::get('/',[HomeController::class,'home'] )->name('home');
Route::get('/cars',[HomeController::class,'cars'] )->name('cars');
Route::get('/types',[HomeController::class,'types'] )->name('types');
Route::get('/options',[HomeController::class,'options'] )->name('options');
Route::get('/pilotes',[HomeController::class,'pilotes'] )->name('pilotes');

Route::get('/car/{id}/show',[CarController::class,'show'])->name('cars.show');
Route::get('/car/create',[CarController::class,'create'])->name('cars.create');
Route::post('/car/create',[CarController::class,'store'])->name('cars.create');
Route::delete("/car/{cars}",[CarController::class,'delete'])->name('cars.delete');
Route::get('/car/{id}/edit',[CarController::class,'edit'])->name("cars.edit");
Route::put('/car/{id}/edit',[CarController::class,'update'])->name("cars.update");

Route::get('/type/{id}/show',[TypeController::class,'show'])->name("types.show");
Route::get('/type/create',[TypeController::class,'create'])->name('types.create');
Route::post('/type/create',[TypeController::class,'store'])->name('types.create');
Route::get('/type/{id}/edit',[TypeController::class,'edit'])->name("types.edit");
Route::put('/type/{id}/edit',[TypeController::class,'update'])->name("types.update");
Route::delete("/type/delete",[TypeController::class,'delete'])->name('types.delete');

Route::get('/option/{id}/show',[OptionController::class,'show'])->name("options.show");
Route::get('/option/create',[OptionController::class,'create'])->name('options.create');
Route::post('/option/create',[OptionController::class,'store'])->name('options.create');
Route::get('/option/{id}/edit',[OptionController::class,'edit'])->name("options.edit");
Route::put('/option/{id}/edit',[OptionController::class,'update'])->name("options.update");
Route::delete("/option/delete",[OptionController::class,'delete'])->name('options.delete');


Route::get('/pilote/{id}/show',[PiloteController::class,'show'])->name('pilotes.show');
Route::get('/pilote/create',[PiloteController::class,'create'])->name('pilotes.create');
Route::post('/pilote/create',[PiloteController::class,'store'])->name('pilotes.create');
Route::delete("/pilote/{pilotes}",[PiloteController::class,'delete'])->name('pilotes.delete');
Route::get('/pilote/{id}/edit',[PiloteController::class,'edit'])->name("pilotes.edit");
Route::put('/pilote/{id}/edit',[PiloteController::class,'update'])->name("pilotes.update");
