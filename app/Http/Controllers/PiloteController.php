<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\pilote;
use Illuminate\Http\Request;

class PiloteController extends Controller
{
    public function create()
    {
        $cars = Car::all();
        return view('pilote.create', ['cars' => $cars]);
    }


    public function store(Request $request)
    {
        $pilote = Pilote::create($request->all());
        $pilote->cars()->sync($request->cars);
        return redirect()->route('pilotes')->with("success", "Pilote ajouté avec succès !");
    }

    public function delete(Pilote $pilote)
    {
        dd($pilote);
        $pilote->cars()->detach();
        $pilote->delete();
        return redirect()->route('pilotes')->with("successDelete", "Pilote supprimé avec succès !");
    }
}
