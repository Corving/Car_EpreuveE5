<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Option;
use App\Models\Type;
use Illuminate\Http\Request;

class CarController extends Controller
{

    public function show($id)
    {
        $cars = Car::findOrFail($id);
        return view('car.show', ['cars' => $cars]);
    }

    public function create()
    {
        $types = Type::all();
        $options = option::all();
        return view('car.create', ['types' => $types, 'options' => $options]);
    }

    public function store(Request $request)
    {
        $cars = Car::create($request->all());
        $cars->options()->sync($request->options);
        return redirect()->route('cars')->with("success", "Voiture ajouté avec succès !");
    }

    public function edit($id)
    {
        $types = Type::all();
        $options = Option::all();
        $cars = Car::findOrFail($id);
        return view('car.edit', ['cars' => $cars, 'types' => $types, 'options' => $options]);
    }

    public function update($id, Request $request)
    {
        $cars = Car::findOrFail($id);
        $cars->update($request->all());
        $cars->options()->sync($request->options);
        return back()->with("success", "Voiture modifié avec succès !");
    }

    public function delete(Car $cars)
    {
        $cars->Options()->detach();
        $cars->delete();
        return redirect()->route('cars')->with("successDelete", "Véhicule supprimé avec succès !");
    }
}
