<?php

namespace App\Http\Controllers;

use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class OptionController extends Controller
{
    public function show($id)
    {
        $options = Option::findOrFail($id);
        return view('option.show', ['options' => $options]);
    }

    public function create()
    {
        $options = option::all();
        return view('option.create', ['options' => $options]);
    }

    public function store(Request $request)
    {
        Option::create($request->all());
        return back()->with("success", "Option ajouté avec succès !");
    }

    public function edit($id)
    {
        $options = Option::findOrFail($id);
        return view('option.edit', ['options' => $options]);
    }

    public function update($id, Request $request)
    {
        $options = Option::findOrFail($id);
        $options->update($request->all());
        return back()->with("success", "Option modifié avec succès !");
    }

    public function delete(Request $request)
    {

        $options = Option::find($request->input('id'));
        try {
            $options->destroy($request->input('id'));
            return redirect('/options');
        } catch (\Throwable $e) {
            return Redirect::back()->withErrors(['msg' => 'L\'option ne peut pas être supprimée car elle est liée à un ou plusieurs véhicules ']);
        }

    }
}
