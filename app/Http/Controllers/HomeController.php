<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Option;
use App\Models\pilote;
use App\Models\Type;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function home()
    {
      return view('home.home');
    }

    public function cars()
    {
        $nbCars=Car::all()->count();
        $cars = Car::orderBy("marque", "asc")->paginate(5);
        return view('car.index', ['nbCars'=>$nbCars,'cars' => $cars]);
    }

    public function types()
    {
        $types = Type::orderBy("libelle", "asc")->paginate(5);
        return view('type.index', ['types' => $types]);
    }

    public function options()
    {
        $options = Option::orderBy("libelle", "asc")->paginate(5);
        return view('option.index', ['options' => $options]);
    }

    public function pilotes()
    {
        $pilotes = pilote::orderBy("nom", "asc")->paginate(5);
        return view('pilote.index', ['pilotes' => $pilotes]);
    }


}
