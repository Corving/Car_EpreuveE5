<?php

namespace App\Http\Controllers;

use App\Models\Type;
use Illuminate\Http\Request;
use \GrahamCampbell\ResultType\Successl;
use Illuminate\Support\Facades\Redirect;

class TypeController extends Controller
{
    public function show($id)
    {
        $types = Type::findOrFail($id);
        return view('type.show', ['types' => $types]);
    }

    public function create()
    {
        $types = Type::all();
        return view('type.create',['types' => $types]);
    }

    public function store(Request $request)
    {
        Type::create($request->all());
        return back()->with("success","Type de véhicule ajouté avec succès !");
    }

    public function edit($id)
    {
        $types = Type::findOrFail($id);
        return view('type.edit', ['types' => $types]);
    }

    public function update($id, Request $request)
    {
        $types = Type::findOrFail($id);
        $types->update($request->all());
        return back()->with("success", "Type de véhicule modifié avec succès !");
    }

    public function delete(Request $request)
    {
        $types = Type::find($request->input('id'));
        try {
            $types->destroy($request->input('id'));
            return redirect('/types');
        } catch (\Throwable $e  ) {
            return Redirect::back()->withErrors(['msg' => 'Le type ne peut pas être supprimé car elle est liée à un ou plusieurs véhicules ']);
        }
    }

}
