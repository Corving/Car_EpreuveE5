<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    protected $fillable = ["libelle"];

    protected $attributes = [
        'libelle' => "Aucune",
    ];


    public function cars()
    {
        return $this->belongsToMany(Car::class);
    }
}
