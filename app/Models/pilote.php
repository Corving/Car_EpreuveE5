<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pilote extends Model
{
    use HasFactory;
    protected $fillable = ['nom','prenom','email','date_de_naissance'];

    public function cars()
    {
        return $this->belongsToMany(Car::class);
    }
}
