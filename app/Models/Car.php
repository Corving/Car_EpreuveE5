<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    private static mixed $request;
    protected $fillable = ['immatriculation','marque','modele','couleur','type_id'];

    public function options()
    {
        return $this->belongsToMany(Option::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function pilotes()
    {
        return $this->belongsToMany(pilote::class);
    }
}
