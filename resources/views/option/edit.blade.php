@extends('base')

@section('title', 'OptionEdit')

@section('content')
    <div class="container-fluid">
        <div class="row no-gutter">

            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-auto mx-auto">
                                @if(session()->has("success"))
                                    <div class="alerte alert-success">
                                        <h3>{{session()->get('success')}}</h3>
                                    </div>
                                @endif
                                <h3 class="display-4">Modifier l'option : {{$options->libelle}}</h3>
                                <p class="text-muted mb-4">Veuillez modifier le formulaire</p>

                                <form method="POST" action="">
                                    @csrf
                                    <input type="hidden" name="_method" value="PUT">


                                    <div class="form-group mb-3">
                                        <input id="libelle" name="libelle" type="text"
                                               placeholder="Libellé"
                                               required="" autofocus="" value="{{$options->libelle}}"
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>

                                    <button type="submit"
                                            class="btn btn-dark btn-block text-uppercase mb-2 rounded-pill shadow-sm">
                                        Enregistrer
                                    </button>

                                    <a href="{{route('options')}}"
                                       class="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Annuler</a>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6 d-none d-md-flex bg-optionEdit"></div>
        </div>
    </div>
@endsection
@section('contentScript')
    <script>

        @if(session()->has("success"))
        Swal.fire(
            'Option Id {{$options->id}}',
            'Modifié avec succès !',
            'success'
        )
        @endif

    </script>
@endsection
