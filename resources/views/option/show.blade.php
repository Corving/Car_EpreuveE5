@extends('base')

@section('title', 'Show-Types')

@section('content')
    <div class="container-fluid">
        <div class="row no-gutter">

            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-auto mx-auto">
                                <h3 class="display-4 mb-4">Option : {{$options->libelle}}</h3>

                                <form method="POST" action="">
                                    <div class="form-group mb-3">
                                        <input id="libelle" name="libelle" type="text"
                                               placeholder="Libellé" value="{{$options->libelle}}" disabled
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>

                                    <a href="{{route('options.edit',[$options->id])}}"
                                       class="btn btn-dark btn-block text-uppercase mb-2 rounded-pill shadow-sm">Modifier</a>

                                    <a href="{{route('options')}}"
                                       class="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Annuler</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-none d-md-flex bg-optionShow"></div>
        </div>
    </div>
@endsection
