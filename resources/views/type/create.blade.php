@extends('base')

@section('title', 'TypeCreate')

@section('content')
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-typeCreate"></div>

            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-8 mx-auto">
                                @if(session()->has("success"))
                                    <div class="alerte alert-success">
                                        <h3>{{session()->get('success')}}</h3>
                                    </div>
                                @endif
                                <h3 class="display-4">Ajout d'un type de Véhicule</h3>
                                <p class="text-muted mb-4">Veuillez renseigner le formulaire</p>

                                <form method="POST" action="{{route('types.create')}}">
                                    @csrf

                                    <div class="form-group mb-3">
                                        <input id="libelle" name="libelle" type="text"
                                               placeholder="Libellé"
                                               required="" autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>

                                    <button type="submit"
                                            class="btn btn-dark btn-block text-uppercase mb-2 rounded-pill shadow-sm">
                                        Enregistrer
                                    </button>

                                    <a href="{{route('types')}}"
                                       class="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Annuler</a>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('contentScript')
    <script>
        @if(session()->has("success"))
        Swal.fire(
            'Type',
            'Ajouté avec succès !',
            'success'
        )
        @endif
    </script>
@endsection
