@extends('base')

@section('title', 'CarÉdition')

@section('content')

    <div class="container-fluid">
        <div class="row no-gutter">

            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-9 mx-auto">
                                @if(session()->has("success"))
                                    <div class="alerte alert-success">
                                        <h3>{{session()->get('success')}}</h3>
                                    </div>
                                @endif
                                <h3 class="display-4 mb-6">Véhicule : {{$cars->immatriculation}}</h3>

                                <form method="POST" action="">

                                    @csrf

                                    <input type="hidden" name="_method" value="PUT">

                                    <div class="form-group mb-3">
                                        <input id="immatriculation" name="immatriculation" type="text"
                                               placeholder="Immatriculation"
                                               required="" autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->immatriculation}}" maxlength="7">
                                    </div>

                                    <div class="form-group mb-3">
                                        <input id="marque" type="text" name="marque" placeholder="Marque" required=""
                                               autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->marque}}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <input id="modele" type="modele" name="modele" placeholder="Modele" required=""
                                               autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->modele}}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <input id="couleur" type="couleur" name="couleur" placeholder="Couleur"
                                               required=""
                                               autofocus="" class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->couleur}}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label class="text-muted mb-1">Choisissez le type de véhicule</label>
                                        <select name="type_id" id="type"
                                                class="form-control rounded-pill border-0 shadow-sm px-4">
                                            @foreach($types as $type)
                                                @if($type->id == $cars->type_id)
                                                    <option name="type_id" value="{{$type->id}}"
                                                            selected>{{$type->libelle}}</option>
                                                @else
                                                    <option name="type_id"
                                                            value="{{$type->id}}">{{$type->libelle}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group mb-3">
                                        <label class="text-muted mb-1">Choisissez les options du véhicule</label>
                                        <select name="options[]" id="options"
                                                class="form-control rounded-pill border-0 shadow-sm px-4" multiple>
                                            @foreach($options as $option)
                                                @if($option->id == $cars->options->contains($option->id))
                                                    <option name="options_id" value="{{$option->id}}"
                                                            selected>{{$option->libelle}}</option>
                                                @else
                                                    <option name="options_id"
                                                            value="{{$option->id}}">{{$option->libelle}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <button type="submit"
                                            class="btn btn-dark btn-block text-uppercase mb-2 rounded-pill shadow-sm">
                                        Enregistrer
                                    </button>

                                    <a href="{{route('cars')}}"
                                       class="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Annuler</a>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6 d-none d-md-flex bg-imageEditCar"></div>

        </div>
    </div>

@endsection

@section('contentScript')
    <script>

        @if(session()->has("success"))
        Swal.fire(
            'Véhicule Immatriculation {{$cars->id}}',
            'Modifié avec succès !',
            'success'
        )
        @endif

    </script>
@endsection
