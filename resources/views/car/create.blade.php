@extends('base')

@section('title', 'CarCreate')

@section('content')
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>

            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-8 mx-auto">
                                @if(session()->has("success"))
                                    <div class="alerte alert-success">
                                        <h3>{{session()->get('success')}}</h3>
                                    </div>
                                @endif
                                <h3 class="display-4">Ajout d'un Véhicule</h3>
                                <p class="text-muted mb-4">Veuillez renseigner le formulaire</p>

                                <form method="POST" action="{{route('cars.create')}}">
                                    @csrf

                                    <div class="form-group mb-3">
                                        <input id="immatriculation" name="immatriculation" type="text"
                                               placeholder="Immatriculation"
                                               required="" autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4" maxlength="7">
                                    </div>
                                    <div class="form-group mb-3">
                                        <input id="marque" type="text" name="marque" placeholder="Marque" required=""
                                               autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>
                                    <div class="form-group mb-3">
                                        <input id="modele" type="modele" name="modele" placeholder="Modèle" required=""
                                               autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>
                                    <div class="form-group mb-3">
                                        <input id="couleur" type="couleur" name="couleur" placeholder="Couleur"
                                               required=""
                                               autofocus="" class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label class="text-muted mb-1">Choisissez le type de véhicule</label>
                                        <select name="type_id" id="type"
                                                class="form-control rounded-pill border-0 shadow-sm px-4">
                                            @foreach($types as $type)
                                                <option name="type_id" value="{{$type->id}}">{{$type->libelle}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group mb-3">
                                        <label class="text-muted mb-1">Choisissez les options du véhicule</label>
                                        <select name="options[]" id="options"
                                                class="form-control rounded-pill border-0 shadow-sm px-4" multiple>
                                            @foreach($options as $option)
                                                <option name="options"
                                                        value="{{$option->id}}">{{$option->libelle}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button type="submit"
                                            class="btn btn-dark btn-block text-uppercase mb-2 rounded-pill shadow-sm">
                                        Enregistrer
                                    </button>

                                    <a href="{{route('cars')}}"
                                       class="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Annuler</a>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('contentScript')
    <script>
        @if(session()->has("success"))
        Swal.fire(
            'Car',
            'Ajouté avec succès !',
            'success'
        )
        @endif
    </script>
@endsection
