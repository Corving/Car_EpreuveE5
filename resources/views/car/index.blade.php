@extends('base')

@section('title', 'Car')

@section('content')
    <br>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 justify-content-around">
                <div class="card">
                    <div class="card-header text-center "><h2>Enregistrement d'un Véhicule</h2></div>
                    <div class="card-body">
                        @if(session()->has("successDelete"))
                            <div class="alerte alert-success">
                                <h3>{{session()->get('successDelete')}}</h3>
                            </div>
                        @endif

                        <a href="{{route('cars.create')}}" class="align-middle button" title="create car">
                            <button class="btn btn-secondary buttonCreate btn-sm align-middle">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-journal-plus" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                          d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5z"/>
                                    <path
                                        d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                                    <path
                                        d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                                </svg>
                                Création d'un véhicule
                            </button>
                        </a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <h3>Nombre de véhicules : {{$nbCars}}</h3>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th class="text-center" scope="col">Immatriculation</th>
                                    <th class="text-center" scope="col">Marque</th>
                                    <th class="text-center" scope="col">Modèle</th>
                                    <th class="text-center" scope="col">Couleur</th>
                                    <th class="text-center" scope="col">Type</th>
                                    <th class="text-center" scope="col">Option</th>
                                    <th class="text-center" scope="col">Pilote</th>
                                    <th class="text-center" scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cars as $car)
                                    <tr>
                                        <td class="text-center align-middle">{{ $loop->iteration }}</td>
                                        <td class="text-center align-middle">{{$car->immatriculation}}</td>
                                        <td class="text-center align-middle">{{$car->marque}}</td>
                                        <td class="text-center align-middle">{{$car->modele}}</td>
                                        <td class="text-center align-middle">{{$car->couleur}}</td>
                                        <td class="text-center align-middle">{{$car->type->libelle}}</td>
                                        <td class="text-center align-middle">
                                            @foreach($car->options as $option)
                                                <li>{{$option->libelle}}</li>
                                        @endforeach
                                        <td class="text-center align-middle">
                                            <a href="{{route('cars.show',[$car->id])}}" class="align-middle button"
                                               title="View cars">
                                                <button class="btn btn-dark btn-sm">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-journal-plus"
                                                         viewBox="0 0 16 16">
                                                        <path fill-rule="evenodd"
                                                              d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5z"/>
                                                        <path
                                                            d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>
                                                        <path
                                                            d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>
                                                    </svg>
                                                    Détails
                                                </button>
                                            </a>
                                            <a href="{{route('cars.edit',[$car->id])}}" class="align-middle button"
                                               title="Edit cars">
                                                <button class="btn btn-secondary btn-sm">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-pencil-square"
                                                         viewBox="0 0 16 16">
                                                        <path
                                                            d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                        <path fill-rule="evenodd"
                                                              d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                    </svg>
                                                    Édition
                                                </button>
                                            </a>
                                            <a href="#" title="Delete car" class="align-middle button"
                                               onclick="if(confirm('Voulez-vous vraiment supprimer ce véhicule ?')){document.getElementById('car-{{$car->id}}').submit() }">
                                                <button class="btn btn-danger btn-sm">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" class="bi bi-trash3-fill"
                                                         viewBox="0 0 16 16">
                                                        <path
                                                            d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                                                    </svg>
                                                    Supprimer
                                                </button>
                                            </a>


                                            <form id="car-{{$car->id}}" action="{{route('cars.delete',[$car->id])}}"
                                                  method="post">
                                                @csrf
                                                <input type="hidden" name="_method" value="delete">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$cars->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
