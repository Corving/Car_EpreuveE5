@extends('base')

@section('title', 'Show-Types')

@section('content')
    <div class="container-fluid mt-5">
        <div class="row no-gutter">

            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-9 mx-auto">
                                @if(session()->has("success"))
                                    <div class="alerte alert-success">
                                        <h3>{{session()->get('success')}}</h3>
                                    </div>
                                @endif
                                <h3 class="display-4 mb-6">Véhicule : {{$cars->immatriculation}}</h3>

                                <form method="POST" action="">

                                    @csrf

                                    <input type="hidden" name="_method" value="PUT">

                                    <div class="form-group mb-3">
                                        <input id="immatriculation" name="immatriculation" type="text"
                                               placeholder="Immatriculation"
                                               disabled
                                               class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->immatriculation}}" maxlength="7">
                                    </div>

                                    <div class="form-group mb-3">
                                        <input id="marque" type="text" name="marque" placeholder="Marque" disabled
                                               class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->marque}}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <input id="modele" type="modele" name="modele" placeholder="Modele" disabled
                                               class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->modele}}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <input id="couleur" type="couleur" name="couleur" placeholder="Couleur"
                                               disabled class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->couleur}}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <input id="couleur" type="couleur" name="couleur" placeholder="Couleur"
                                               disabled class="form-control rounded-pill border-0 shadow-sm px-4"
                                               value="{{$cars->type->libelle}}">
                                    </div>

                                    <div class="form-group mb-3">
                                        <select name="options[]" id="options"
                                                class="form-control rounded-pill border-0 shadow-sm px-4" multiple>
                                            @foreach($cars->options as $option)
                                                @if($option->id == $cars->options->contains($option->id))
                                                    <option name="options_id" value="{{$option->id}}"
                                                            selected>{{$option->libelle}}</option>
                                                @else
                                                    <option name="options_id"
                                                            value="{{$option->id}}">{{$option->libelle}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <a href="{{route('cars.edit',[$cars->id])}}"
                                       class="btn btn-dark btn-block text-uppercase mb-2 rounded-pill shadow-sm">Modifier</a>

                                    <a href="{{route('cars')}}"
                                       class="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Annuler</a>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6 d-none d-md-flex bg-carShow"></div>

        </div>
    </div>

@endsection
