@extends('base')

@section('title', 'PiloteCreate')

@section('content')
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>

            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">

                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-8 mx-auto">
                                @if(session()->has("success"))
                                    <div class="alerte alert-success">
                                        <h3>{{session()->get('success')}}</h3>
                                    </div>
                                @endif
                                <h3 class="display-4">Ajout d'un Pilote</h3>
                                <p class="text-muted mb-4">Veuillez renseigner le formulaire</p>

                                <form method="POST" action="{{route('pilotes.create')}}">
                                    @csrf

                                    <div class="form-group mb-3">
                                        <input id="nom" name="nom" type="text"
                                               placeholder="Nom"
                                               required="" autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>
                                    <div class="form-group mb-3">
                                        <input id="prenom" name="prenom" type="text"
                                               placeholder="Prénom"
                                               required="" autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>
                                    <div class="form-group mb-3">
                                        <input id="email" type="email" name="email" placeholder="Email" required=""
                                               autofocus=""
                                               class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>
                                    <div class="form-group mb-3">
                                        <input id="date_de_naissance" type="date" name="date_de_naissance" placeholder="Date de naissance"
                                               required=""
                                               autofocus="" class="form-control rounded-pill border-0 shadow-sm px-4">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label class="text-muted mb-1">Choisissez les véhicule</label>
                                        <select name="cars[]" id="cars"
                                                class="form-control rounded-pill border-0 shadow-sm px-4" multiple>
                                            @foreach($cars as $car)
                                                <option name="cars"
                                                        value="{{$car->id}}">{{$car->immatriculation.' // '.$car->marque}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button type="submit"
                                            class="btn btn-dark btn-block text-uppercase mb-2 rounded-pill shadow-sm">
                                        Enregistrer
                                    </button>

                                    <a href="{{route('cars')}}"
                                       class="btn btn-secondary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Annuler</a>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('contentScript')
    <script>
        @if(session()->has("success"))
        Swal.fire(
            'Car',
            'Ajouté avec succès !',
            'success'
        )
        @endif
    </script>
@endsection
