<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_pilote', function (Blueprint $table) {
            $table->foreignId('car_id')->constrained('cars');
            $table->foreignId('pilote_id')->constrained('pilotes');
            $table->primary(['car_id', 'pilote_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_pilote');
    }
};
