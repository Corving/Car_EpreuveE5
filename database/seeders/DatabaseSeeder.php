<?php

namespace Database\Seeders;

use App\Models\Option;
use Illuminate\Database\Seeder;
use \App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        DB::table('options')->insert([
            [
                'id' => 1,
                'libelle' => 'GPS',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'libelle' => 'Airbarg',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);

        DB::table('types')->insert([
            [
                'id' => 1,
                'libelle' => 'Coupé',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 2,
                'libelle' => 'Berline',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 3,
                'libelle' => 'Hayon',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 4,
                'libelle' => 'Break',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 5,
                'libelle' => 'Limousine',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 6,
                'libelle' => 'Pick-up',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 7,
                'libelle' => 'Crossover',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 8,
                'libelle' => 'Suv',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 9,
                'libelle' => 'Fourgonnette',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 10,
                'libelle' => 'Cabriolet',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 11,
                'libelle' => 'roadster',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ],
            ['id' => 12,
                'libelle' => 'Minibus',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

            ]

        ]);


        DB::table('cars')->insert([
            [
                'id' => 1,
                'immatriculation' => 'bd123az',
                'marque' => 'ford',
                'modele' => 'fiesta',
                'couleur' => 'Bleu',
                'type_id' => random_int(1, 12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'immatriculation' => 'az643sq',
                'marque' => 'ford',
                'modele' => 'focus',
                'couleur' => 'Rouge',
                'type_id' => random_int(1, 12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'immatriculation' => 'lk291az',
                'marque' => 'audi',
                'modele' => 'e-tron',
                'couleur' => 'Noir',
                'type_id' => random_int(1, 12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);

        DB::table('car_option')->insert([
            [
                'car_id' => 1,
                'option_id' => 1
            ],
            [
                'car_id' => 2,
                'option_id' => 2
            ],
            [
                'car_id' => 3,
                'option_id' => 1
            ],
            [
                'car_id' => 3,
                'option_id' => 2
            ]
        ]);

    }
}
